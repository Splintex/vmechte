$(document).ready(function() {

	"use strict";
	
	document.createElement( "picture" );

	var promoSlider = $(".js-promo-slider");
	promoSlider.on('init', function(){
		setTimeout(function(){
			promoSlider.addClass("is-ready");
		}, 200)
	});
	promoSlider.slick({
		fade: true,
		autoplay: true,
		autoplaySpeed: 5000,
		arrows: true,
		dots: true
	});

	// if ($('.js-scroll').length) {
	// 	$('.js-scroll').perfectScrollbar();
	// }
	$('.js-scroll').perfectScrollbar();

	
	$(".js-box-slider").slick({
		infinite: false,
		slidesToShow: 3,
		slidesToScroll: 3,
		prevArrow: '<button class="btn-prev" type="button"><i class="icon-caret-l"></i></button>',
		nextArrow: '<button class="btn-next" type="button"><i class="icon-caret-r"></i></button>',
		responsive: [
			{
				breakpoint: 981,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 721,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					dots: true
				}
			}
		]
	});

	$(".js-crsl-info").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		arrows: true,
		adaptiveHeight: true,
	});

	$(".js-rank-slider").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		arrows: false,
		adaptiveHeight: true,
		infinite: false
	});

	var sliderIn = $(".js-slider-in");
	var money = $(".js-money");
	var moneyMax = money.data("max");
	var moneyGive = $(".js-money-give");
	var moneyGiveSum = $(".js-money-give span");
	var moneyGet = $(".js-money-get");
	var moneyGetSum = $(".js-money-get span");

	function writeMoneyGive(sum) {
		moneyGiveSum.text(sum);
	}

	function writeMoneyGet(sum) {
		moneyGetSum.text(sum);
	}

	function makeGiveColumn(sum) {
		var percent = (sum/moneyMax)*100;
		moneyGive.css({
			height: percent+"%"
		});
		moneyGet.css({
			bottom: percent+"%"
		});
	}

	function makeGetColumn(sum) {
		var percent = (sum/moneyMax)*100;
		moneyGet.css({
			height: percent+"%"
		});
	}

	sliderIn.each(function() {
		sliderIn = $(this);
		var term = 30;

		var sliderInMin = sliderIn.data("min");
		var sliderInMax = sliderIn.data("max");
		var sliderInStep = sliderIn.data("step");
		var sliderInValue = sliderInMin;

		var percents = sliderIn.data("percents").split(",");
		var values = sliderIn.data("values").split(",");
		var kMin = percents[0]*term/100;
		var kMax = percents[percents.length-1]*term/100;

		var sliderOut = $('[data-slider="'+$(this).data("connect")+'"]');
		var sliderOutMin = sliderInMin*kMin;
		var sliderOutMax = sliderInMax*kMax;
		var sliderOutStep = sliderOut.data("step");
		var sliderOutValue = sliderOutMin;

		var outValues = [];
		for (var i = 0; i < values.length; i++) {
			outValues[i] = values[i]*percents[i]*term/100
		}
		
		
		sliderOut.attr("data-values", outValues);

		sliderIn.slider({
			range: "min",
			min: sliderInMin,
			max: sliderInMax,
			step: sliderInStep,
			value: sliderInValue,
			slide: function( event, ui ) {
				var resultIn = ui.value;
				var percent;
				$(ui.handle).text(formating(resultIn));
				for (var i = 0; i < values.length; i++) {
					if (resultIn <= values[i]) {
						percent = percents[i];
						break;
					}
				}
				var k = percent/100;
				var resultOut = resultIn*k*term;
				connectSlider(sliderOut, resultOut);
				writeMoneyGive(formating(resultIn));
				writeMoneyGet(formating(Math.round10(resultOut, 0)));
				makeGiveColumn(resultIn);
				makeGetColumn(resultOut);
			}
		});

		var sliderInVal = sliderIn.slider("value");
		sliderIn.find(".ui-slider-handle").text(formating(sliderInVal));

		sliderOut.slider({
			range: "min",
			min: sliderOutMin,
			max: sliderOutMax,
			step: sliderOutStep,
			value: sliderOutValue,
			slide: function(event,ui) {
				var resultIn = ui.value;
				var percent;
				$(ui.handle).text(formating(resultIn));
				for (var i = 0; i < outValues.length; i++) {
					if (resultIn <= outValues[i]) {
						percent = percents[i];
						break;
					}
				}
				var k = percent/100;
				var resultOut = resultIn/k/term;
				connectSlider(sliderIn, resultOut);
				writeMoneyGet(formating(resultIn));
				writeMoneyGive(formating(Math.round10(resultOut, 0)));
				makeGiveColumn(resultOut);
				makeGetColumn(resultIn);
			}
		});
		var sliderOutVal = sliderOut.slider("value");
		sliderOut.find(".ui-slider-handle").text(formating(sliderOutVal));
	});
	

	(function() {
	  /**
	   * Корректировка округления десятичных дробей.
	   *
	   * @param {String}  type  Тип корректировки.
	   * @param {Number}  value Число.
	   * @param {Integer} exp   Показатель степени (десятичный логарифм основания корректировки).
	   * @returns {Number} Скорректированное значение.
	   */
	  function decimalAdjust(type, value, exp) {
	    // Если степень не определена, либо равна нулю...
	    if (typeof exp === 'undefined' || +exp === 0) {
	      return Math[type](value);
	    }
	    value = +value;
	    exp = +exp;
	    // Если значение не является числом, либо степень не является целым числом...
	    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
	      return NaN;
	    }
	    // Сдвиг разрядов
	    value = value.toString().split('e');
	    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
	    // Обратный сдвиг
	    value = value.toString().split('e');
	    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
	  }

	  // Десятичное округление к ближайшему
	  if (!Math.round10) {
	    Math.round10 = function(value, exp) {
	      return decimalAdjust('round', value, exp);
	    };
	  }
	  // Десятичное округление вниз
	  if (!Math.floor10) {
	    Math.floor10 = function(value, exp) {
	      return decimalAdjust('floor', value, exp);
	    };
	  }
	  // Десятичное округление вверх
	  if (!Math.ceil10) {
	    Math.ceil10 = function(value, exp) {
	      return decimalAdjust('ceil', value, exp);
	    };
	  }
	})();

	$(".js-fancybox a").fancybox({
		helpers : {
			overlay: {
				locked: false
			}
		},
		nextEffect: 'fade',
		prevEffect: 'fade',
		openEffect: 'fade',
		closeEffect: 'fade',
		padding: 0
	});


	function formating(str) {
		var str = "" + str;
		if (str.length < 4) {
			return str;
		}
		if (str.length == 4) {
			var zeroPart = str.slice(-3);
			var mainPart = str.slice(0);
		}
		if (str.length <= 6) {
			var zeroPart = str.slice(-3);
			var mainPart = str.slice(0, str.length-3);
		}
		if (str.length > 6) {
			var zeroPart = str.slice(1,4) + " " + str.slice(-3);
			var mainPart = str.slice(0,1);
		}
		str = mainPart + " " + zeroPart;
		return str;
	}

	function connectSlider(slider, value) {
		slider.slider("value", value);
		//slider.find(".ui-slider-handle").text(formating(Math.floor(value)));
		slider.find(".ui-slider-handle").text(formating(Math.round10(value, 0)));
	}

	$( "#from" ).datepicker({
      changeMonth: true,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#to" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#to" ).datepicker({
      changeMonth: true,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });



});