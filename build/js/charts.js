$(document).ready(function() {

	var ctx1 = document.getElementById("myChart1");
	var myChart = new Chart(ctx1, {
	    type: 'line',
	    defaultFontColor: "#fff",
	    defaultFontSize: 14,
	    scaleFontColor: "#ff0000",
	    labels: {
	    	fontColor: "#fff"
	    },
	    data: {
	        labels: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
	        datasets: [{
	        	label: "Оборот структуры",
	            data: [10000, 20000, 30000, 40000, 50000, 80000, 100000, 200000, 500000, 800000, 900000, 1000000],
	        	backgroundColor: "#5584b5",
	        	borderColor: "#a3d6f0",
	        	borderWidth: 2,
	        	pointBorderColor: "#fff",
	        	pointBackgroundColor: "#7fb2f1",

	        }]
	    },
	    title: {
	    	display: false
	    },
	    options: {
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true
	                },
	                gridLines: {
	                	display: false
	                }
	            }],
	            xAxes: [{
	                gridLines: {
	                	display: false
	                }
	            }],
	        }
	    }
	});

	var ctx2 = document.getElementById("myChart2");
	var myChart = new Chart(ctx2, {
	    type: 'line',
	    defaultFontColor: "#fff",
	    defaultFontSize: 14,
	    scaleFontColor: "#ff0000",
	    labels: {
	    	fontColor: "#fff"
	    },
	    data: {
	        labels: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
	        datasets: [{
	        	label: "Оборот структуры",
	            data: [10000, 20000, 30000, 40000, 50000, 80000, 100000, 200000, 500000, 800000, 900000, 1000000],
	        	backgroundColor: "#5584b5",
	        	borderColor: "#a3d6f0",
	        	borderWidth: 2,
	        	pointBorderColor: "#fff",
	        	pointBackgroundColor: "#7fb2f1",

	        }]
	    },
	    title: {
	    	display: false
	    },
	    options: {
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true
	                },
	                gridLines: {
	                	display: false
	                }
	            }],
	            xAxes: [{
	                gridLines: {
	                	display: false
	                }
	            }],
	        }
	    }
	});

	var ctx3 = document.getElementById("myChart3");
	var myChart = new Chart(ctx3, {
	    type: 'line',
	    defaultFontColor: "#fff",
	    defaultFontSize: 14,
	    scaleFontColor: "#ff0000",
	    labels: {
	    	fontColor: "#fff"
	    },
	    data: {
	        labels: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
	        datasets: [{
	        	label: "Оборот структуры",
	            data: [10000, 20000, 30000, 40000, 50000, 80000, 100000, 200000, 500000, 800000, 900000, 1000000],
	        	backgroundColor: "#5584b5",
	        	borderColor: "#a3d6f0",
	        	borderWidth: 2,
	        	pointBorderColor: "#fff",
	        	pointBackgroundColor: "#7fb2f1",

	        }]
	    },
	    title: {
	    	display: false
	    },
	    options: {
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true
	                },
	                gridLines: {
	                	display: false
	                }
	            }],
	            xAxes: [{
	                gridLines: {
	                	display: false
	                }
	            }],
	        }
	    }
	});

	var ctx4 = document.getElementById("myChart4");
	var myChart = new Chart(ctx4, {
	    type: 'line',
	    defaultFontColor: "#fff",
	    defaultFontSize: 14,
	    scaleFontColor: "#ff0000",
	    labels: {
	    	fontColor: "#fff"
	    },
	    data: {
	        labels: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
	        datasets: [{
	        	label: "Оборот структуры",
	            data: [10000, 20000, 30000, 40000, 50000, 80000, 100000, 200000, 500000, 800000, 900000, 1000000],
	        	backgroundColor: "#5584b5",
	        	borderColor: "#a3d6f0",
	        	borderWidth: 2,
	        	pointBorderColor: "#fff",
	        	pointBackgroundColor: "#7fb2f1",

	        }]
	    },
	    title: {
	    	display: false
	    },
	    options: {
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true
	                },
	                gridLines: {
	                	display: false
	                }
	            }],
	            xAxes: [{
	                gridLines: {
	                	display: false
	                }
	            }],
	        }
	    }
	});

	var ctx5 = document.getElementById("myChart5");
	var myChart = new Chart(ctx5, {
	    type: 'line',
	    defaultFontColor: "#fff",
	    defaultFontSize: 14,
	    scaleFontColor: "#ff0000",
	    labels: {
	    	fontColor: "#fff"
	    },
	    data: {
	        labels: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
	        datasets: [{
	        	label: "Оборот структуры",
	            data: [10000, 20000, 30000, 40000, 50000, 80000, 100000, 200000, 500000, 800000, 900000, 1000000],
	        	backgroundColor: "#5584b5",
	        	borderColor: "#a3d6f0",
	        	borderWidth: 2,
	        	pointBorderColor: "#fff",
	        	pointBackgroundColor: "#7fb2f1",

	        }]
	    },
	    title: {
	    	display: false
	    },
	    options: {
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true
	                },
	                gridLines: {
	                	display: false
	                }
	            }],
	            xAxes: [{
	                gridLines: {
	                	display: false
	                }
	            }],
	        }
	    }
	});

	var ctx6 = document.getElementById("myChart6");
	var myChart = new Chart(ctx6, {
	    type: 'line',
	    defaultFontColor: "#fff",
	    defaultFontSize: 14,
	    scaleFontColor: "#ff0000",
	    labels: {
	    	fontColor: "#fff"
	    },
	    data: {
	        labels: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
	        datasets: [{
	        	label: "Оборот структуры",
	            data: [10000, 20000, 30000, 40000, 50000, 80000, 100000, 200000, 500000, 800000, 900000, 1000000],
	        	backgroundColor: "#5584b5",
	        	borderColor: "#a3d6f0",
	        	borderWidth: 2,
	        	pointBorderColor: "#fff",
	        	pointBackgroundColor: "#7fb2f1",

	        }]
	    },
	    title: {
	    	display: false
	    },
	    options: {
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true
	                },
	                gridLines: {
	                	display: false
	                }
	            }],
	            xAxes: [{
	                gridLines: {
	                	display: false
	                }
	            }],
	        }
	    }
	});


});