$(document).ready(function() {

	var scrollTo = {
		init: function(options) {
			this.$btn = $(".js-scroll-btn");
			this.$root = $('html, body');
			this.timeout = 500;
			this._bindEvents();
		},
		_bindEvents: function() {
			this.$btn.on("click", this._goTo.bind(this));
		},
		_goTo: function(event) {
			var $btn = $(event.currentTarget)
			var $target = $($btn.data("target"));
			this.$root.animate({
			    scrollTop: $target.offset().top
			}, this.timeout);
			return false;
			
		}
	}
	scrollTo.init();

	var drop = {
		init: function(options) {
			this.$btnSelector = ".js-drop";
			this.$btn = $(this.$btnSelector);
			this.$titleSelector = ".js-drop-title";
			this.$item = this.$btn.find("li");
			this.$root = $(document);
			this._bindEvents();
		},
		_bindEvents: function() {
			this.$btn.on("click", this._showList.bind(this));
			this.$item.on("click", this._changeValue.bind(this));
			this.$root.on("click", this._hideList.bind(this));
		},
		_showList: function(event) {
			var $btn = $(event.currentTarget);
			$btn.toggleClass("is-active");
			event.stopPropagation();
			if (!$btn.hasClass("js-drop-links")) {
				return false;
			}
			
		},
		_hideList: function() {
			this.$btn.removeClass("is-active");
		},
		_changeValue: function(event) {
			var $item = $(event.currentTarget);
			var text = $item.text();
			var index = $item.index();
			this.$parent = $item.parents(this.$btnSelector);
			this.$parent.find(this.$titleSelector).text(text);
			if (this.$parent.hasClass("js-drop-tabs")) {
				this._changeTab(index);
			}
			if (this.$parent.hasClass("js-drop-select")) {
				this._changeSelect(text);
			}
		},
		_changeTab: function(index) {
			var group = this.$parent.data("tabs-group");
			var tabContent = $('[data-group="'+group+'"]').children();
			tabContent.attr("hidden", true);
			tabContent.eq(index).attr("hidden", false);
		},
		_changeSelect: function(text) {
			var $select = this.$parent.find("select");
			$select.val(text);
		}

	}
	drop.init();

	$(".js-toggle-exch").on("click", function() {
		$(".js-exch").toggleClass("is-active");
	});

	$(".js-btn-menu").on("click", function(){
		$(".js-btn-menu").toggleClass("is-active");
		$(".js-menu").toggleClass("is-active")
	});

	$(".js-btn-promo").on("click", function() {
		var target = $(this).data("target");
		$('[data-promo]').attr("hidden", true);
		$('[data-promo="'+target+'"]').attr("hidden", false);
		return false;
	});


	$(".js-close-popup").on("click", function() {
		$(this).parents(".js-popup").removeClass("is-active");
		return false;
	});

	$(".js-open-popup").on("click", function() {
		var $target = $('[data-popup="'+$(this).data("target")+'"]');
		$(".js-popup").removeClass("is-active");
		$target.addClass("is-active");
		if ($(this).data("slide")) {
			var index = $(this).data("slide");
			$(".js-rank-slider").slick("slickGoTo", index);
		}
		return false;
	});
	$(".js-reduct").on("click", function(){
		$(this).parent().find("input").removeAttr("readonly").focus().select();
	});
	$(".js-file .btn-download").on("click", function (){
        $(this).prev().trigger("click");
        return false;
	});

	$(".js-show-parent").on("click", function(){
		$(this).parent().next().toggleClass("is-active");
		$(this).toggleClass("is-active");
		return false;
	});
	
	$(".js-btn-sidebar").on("click",function(){
		$(this).toggleClass("is-active");
		$(".js-sidebar").toggleClass("is-active")
	});
	$(".js-point-reduct").on("click", function(){
		$(this).parents(".point").toggleClass("is-active");
	});
	$(".js-point-cheked").on("click", function(){
		$(this).parents(".point").addClass("is-inactive");
		$(this).parents(".point").removeClass("is-main");
	});
	$(".js-point-main").on("click", function(){
		$(this).parents(".point").toggleClass("is-main");
	});

	$(".js-show-tip").on("click", function(){
		$(this).parents(".tip-parent").toggleClass("is-active");
	});

});